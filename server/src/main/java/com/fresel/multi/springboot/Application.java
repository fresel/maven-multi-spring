package com.fresel.multi.springboot;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import static org.springframework.boot.SpringApplication.run;

/**
 * Main Application class.
 *
 * @author Fredrik Selander
 */
 @SpringBootApplication
public class Application {

    /**
     * Main entry point.
     *
     * @param args application arguments
     */
    public static void main(String[] args) {
      run(Application.class, args);
    }
}
